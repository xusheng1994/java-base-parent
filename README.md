# 参考文章

[JavaEE基础(01)：Servlet实现方式，生命周期执行过程](https://mp.weixin.qq.com/s?__biz=MzU4Njg0MzYwNw==&mid=2247484304&idx=1&sn=dd6b6852e35031dd07f70d441f3ddc85&chksm=fdf45728ca83de3e158597030cf46b1677eccf533f9e1412690cd64b0ec0cce544711ceabddb&token=1248678182&lang=zh_CN#rd)<br/>
[JavaEE基础(02)：Servlet核心API用法详解](https://mp.weixin.qq.com/s?__biz=MzU4Njg0MzYwNw==&mid=2247484309&idx=1&sn=fc8237aef2f246f85b978561f4b37e1e&chksm=fdf4572dca83de3b8affdf4a9a8c8b3c18e4c5203c3e663e0bfaeb53788d3e2d26fa829bb8c6&token=1248678182&lang=zh_CN#rd)<br/>
[JavaEE基础(03)：Http请求详解，握手挥手流程简介](https://mp.weixin.qq.com/s?__biz=MzU4Njg0MzYwNw==&mid=2247484318&idx=1&sn=125780e20ada2f3d1451a1563a1c1e6f&chksm=fdf45726ca83de30adc5956a16d16151825b37c0adf5350214899ade39fef47f17ee35b2390d&token=1248678182&lang=zh_CN#rd)<br/>

持续更新中...

## 项目简介

Java基础类型，容器，并发，IO流，面向对象，Web编程等代码总结。

## 关于作者
【<b>公众号：知了一笑</b>】    【<b><a href="https://www.zhihu.com/people/cicadasmile/columns">知乎专栏</a></b>】<br/>
<img width="255px" height="255px" src="https://avatars0.githubusercontent.com/u/50793885?s=460&v=4"/><br/>

## 推荐项目

|项目名称|GitHub地址|GitEE地址|推荐指数|
|:---|:---|:---|:---|
|Java描述常用设计模式,算法,数据结构|[GitHub·点这里](https://github.com/cicadasmile/model-arithmetic-parent)|[GitEE·点这里](https://gitee.com/cicadasmile/model-arithmetic-parent)|☆☆☆☆☆|
|Java基础、进阶、并发、面向对象、Web开发|[GitHub·点这里](https://github.com/cicadasmile/java-base-parent)|[GitEE·点这里](https://gitee.com/cicadasmile/java-base-parent)|☆☆☆☆|
|SpringCloud微服务架构实战综合案例|[GitHub·点这里](https://github.com/cicadasmile/husky-spring-cloud)|[GitEE·点这里](https://gitee.com/cicadasmile/husky-spring-cloud)|☆☆☆☆☆|
|SpringCloud微服务基础组件案例详解|[GitHub·点这里](https://github.com/cicadasmile/spring-cloud-base)|[GitEE·点这里](https://gitee.com/cicadasmile/spring-cloud-base)|☆☆☆|
|SpringCloud实现分库分表模式下数据库实时扩容|[GitHub·点这里](https://github.com/cicadasmile/cloud-shard-jdbc)|[GitEE·点这里](https://gitee.com/cicadasmile/cloud-shard-jdbc)|☆☆☆☆☆|
|SpringBoot框架基础应用入门到进阶|[GitHub·点这里](https://github.com/cicadasmile/spring-boot-base)|[GitEE·点这里](https://gitee.com/cicadasmile/spring-boot-base)|☆☆☆☆|
|SpringBoot框架整合开发常用中间件|[GitHub·点这里](https://github.com/cicadasmile/middle-ware-parent)|[GitEE·点这里](https://gitee.com/cicadasmile/middle-ware-parent)|☆☆☆☆☆|
|Spring+Mvc框架基础案例详解|[GitHub·点这里](https://github.com/cicadasmile/spring-mvc-parent)|[GitEE·点这里](https://gitee.com/cicadasmile/spring-mvc-parent)|☆☆|
|Linux系统基础、运维,常用操作积累|[GitHub·点这里](https://github.com/cicadasmile/linux-system-base)|[GitEE·点这里](https://gitee.com/cicadasmile/linux-system-base)|☆☆☆|
